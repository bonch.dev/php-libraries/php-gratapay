<?php


namespace BonchDev\PHPGratapay\Tests;


use BonchDev\PHPGratapay\GratapayAPI;
use BonchDev\PHPGratapay\GratapayRequest;
use PHPUnit\Framework\TestCase;

class GratapayAPITest extends TestCase
{
    private $baseUri;
    private $authKey;
    private $secretKey;
    private $paymentSystem;
    private $testCard;

    protected function setUp(): void
    {
        parent::setUp();

        $this->baseUri = "https://gratapay.com/api/";
        $this->authKey = "";    // authKey here
        $this->secretKey = "";  // secretKey here
        $this->paymentSystem = "TestCard";
        $this->testCard = "4111111111111111";
    }

    public function testAPI()
    {
        $gratapay = new GratapayAPI(
            $this->baseUri,
            $this->authKey,
            $this->secretKey
        );

        $gratapay->setRequest(
            new GratapayRequest(
                12.02,
                "RUB",
                $this->paymentSystem,
                "test_id_7",
                [
                    "card_number" => $this->testCard,
                    "client_phone" => "79999999999",
                    "payment_description" => "test payment"
                ]
            )
        );

        var_dump(
            $gratapay->depositCreate()->response()
        );
    }

}