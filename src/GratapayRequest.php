<?php


namespace BonchDev\PHPGratapay;


class GratapayRequest
{
    /** @var float */
    public $amount;
    /** @var string */
    public $currency;
    /** @var string */
    public $paymentSystem;
    /** @var string */
    public $transactionID;
    /** @var string */
    public $note;
    /** @var array */
    public $systemFields;
    /** @var array */
    public $url;

    public function __construct(
        float $amount,
        string $currency,
        string $paymentSystem,
        string $transactionID,
        array $systemFields,
        string $note = null
    ) {
        $this->checkSystemFields($systemFields);

        $this->amount = $amount;
        $this->currency = $currency;
        $this->paymentSystem = $paymentSystem;
        $this->transactionID = $transactionID;
        $this->systemFields = $systemFields;
        $this->note = $note;
    }

    private function checkSystemFields(array $systemFields)
    {
        if (
            !isset($systemFields["card_number"])
            || !isset($systemFields["client_phone"])
            || !isset($systemFields["payment_description"])
        ) {
            throw new \Exception("not all system fields present");
        }
    }

    public function setUrl(array $url)
    {
        $this->url = $url;
    }

    public function toArray(): array
    {
        $array = (array)$this;

        foreach ($array as $key => $value) {
            if ($value == null) {
                unset($array[$key]);

                continue;
            }

            preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $key, $matches);

            $ret = $matches[0];
            foreach ($ret as &$match) {
                $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
            }

            unset($array[$key]);

            $key = implode('_', $ret);

            $array[$key] = $value;
        }

        return $array;
    }

    public function encodedArray()
    {
        return json_encode(
            $this->toArray()
        );
    }
}