<?php


namespace BonchDev\PHPGratapay;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;

class GratapayAPI
{
    /** @var string */
    private $baseUri;
    /** @var array */
    private $headers;
    /** @var string */
    private $authKey;
    /** @var string */
    private $secretKey;
    /** @var GratapayRequest */
    public $request;
    /** @var Response */
    public $response;

    public function __construct(
        string $baseUri,
        string $authKey,
        string $secretKey
    ) {
        $this->baseUri = $baseUri;
        $this->authKey = $authKey;
        $this->secretKey = $secretKey;

        $this->headers["Auth"] = $authKey;
        $this->headers["Accept"] = "application/json";
        $this->headers["Content-Type"] = "application/json";
    }

    private function sign()
    {
        if ($this->request == null) {
            throw new \Exception("empty request");
        }

        $request = $this->request;

        $sign = md5(
            $request->encodedArray() . $this->secretKey
        );

        $this->headers["Sign"] = $sign;
    }

    public function setRequest(GratapayRequest $request)
    {
        $this->request = $request;

        $this->sign();

        return $this;
    }

    public function depositCreate()
    {
        $client = new Client([
            "base_uri" => $this->baseUri,
            "headers" => $this->headers
        ]);

        try {
            $this->response = $client
                ->post("/api/deduce/create", [
                    "body" => $this->request->encodedArray()
                ]);
        } catch (ClientException $exception) {
            $this->response = $exception->getResponse();
        }

        return $this;
    }

    public function response()
    {
        $response = $this->response;

        $responseJson = json_decode(
            $response->getBody()->getContents(), true
        );

        return new GratapayResponse($responseJson);
    }
}