<?php


namespace BonchDev\PHPGratapay;


use Psr\Http\Message\ResponseInterface;

class GratapayResponse
{
    public $status;
    public $id;
    public $transactionID;
    public $type;
    public $amountToPay;
    public $amountMerchant;
    public $amountClient;
    public $currency;
    public $paymentSystem;
    public $redirect;
    public $code;
    public $message;
    public $description;

    public function __construct(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $attribute = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $key))));
            $this->$attribute = $value;
        }
    }
}