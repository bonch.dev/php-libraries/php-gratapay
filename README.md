# PHP Gratapay

PHP package for Gratapay.

## Installation

Run `composer require bonch.dev/php-gratapay`. 

## Usage

There are 3 classes:

- `BonchDev\PHPGratapay\GratapayAPI` – API client for Gratapay
- `BonchDev\PHPGratapay\GratapayRequest` – collect data for requests
- `BonchDev\PHPGratapay\GratapayResponse` – handle response from service

You need `authKey` and `secretKey` for performing requests. 
Provide this to `BonchDev\PHPGratapay\GratapayAPI` constructor.